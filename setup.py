from distutils.core import setup, Extension

module1 = Extension('aug_map',
                    sources = ['aug_mapmodule.cpp'],
                    extra_compile_args=['mcx16'],
                    include_dirs=['../PAM/c++'])

setup (name = 'aug_map',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])
