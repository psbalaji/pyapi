#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include "pam.h"
//#include "../c++/pam.h"

struct pymap {
	using key_t = PyObject*;
	using val_t = PyObject*;
	

	static inline bool comp (const key_t& a, const key_t& b) {
		//PyObject* arglist = Py_BuildValue ("OO", a, b);
		PyObject* result = PyObject_CallMethod(a,"__gt__", "O", b);
		//Py_DECREF(arglist);
		return PyObject_IsTrue(result);
	}
};

using map_class = pam_map<pymap>;

map_class* m1;

PyObject* 
map_create(PyObject* self, PyObject* args) {
	map_class* mc1 = new map_class();
	return Py_BuildValue("l",mc1); 
}

static PyObject*
map_clear(PyObject* self, PyObject* args) {
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "l", &map_addr)) 
		return NULL;
	m1 = (map_class*)map_addr;
	m1->clear();
	Py_RETURN_NONE;
}

static PyObject*
map_insert(PyObject* self, PyObject* args) {
	PyObject* key;
	PyObject* value;
	uint64_t pam_map_addr;

	if (!PyArg_ParseTuple(args, "OOl", &key, &value, &pam_map_addr))
		return NULL;
	map_class* m1 = (map_class*)(pam_map_addr);
	try {
		m1->insert(std::make_pair(key, value));
		Py_INCREF(key);
		Py_INCREF(value);
	} catch (exception& e) {
		std::cout << e.what() << std::endl;
	}
	return Py_BuildValue("O", key);
}

/*
 * Need to handle the when the given key is not present and return Py_RETURN_NONE
 * Also everytime we are returning a python object, we have to increase it's ref
 * count.
 */
static PyObject* 
map_find(PyObject* self, PyObject* args) {
	PyObject* key;
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "Ol", &key, &map_addr))
		return NULL;
	map_class* m1 = (map_class*)map_addr;
	return Py_BuildValue("O", m1->find(key));
}

static PyObject* map_rank (PyObject* self, PyObject* args) {
	PyObject* key;
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "Ol", &key, &map_addr))
		return NULL;
	map_class* m1 = (map_class*)map_addr;
	return Py_BuildValue("l", m1->rank(key));
}

static PyObject* 
map_size (PyObject* self, PyObject* args) {
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "l", &map_addr))
		return NULL;
	map_class* m1 = (map_class*)(map_addr);
	return Py_BuildValue("l", m1->size());
}

static PyObject*
map_is_empty (PyObject* self, PyObject* args) {
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "l", &map_addr))
		return NULL;
	map_class* m1 = (map_class*)map_addr;
	if (m1->size() == 0)
		Py_RETURN_TRUE;
	Py_RETURN_FALSE;
}

static PyObject*
map_contains (PyObject* self, PyObject* args) {
	PyObject* key;
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "l", &key, &map_addr))
		return NULL;
	m1 = (map_class*)map_addr;
	if (m1->contains(key))
		Py_RETURN_TRUE;
	Py_RETURN_FALSE;
}

static PyObject*
map_next (PyObject* self, PyObject* args) {
	PyObject* key;
	map_class::maybe_E entry;
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "Ol", &key, &map_addr))
		return NULL;
	m1 = (map_class*)map_addr;
	entry = m1->next(key);
	// entry is present return a python tuple with key and value
	if (entry) {
		PyObject* next_key = (*entry).first;
		PyObject* next_val = (*entry).second;
		return Py_BuildValue("(OO)", next_key, next_val);
	}
	Py_RETURN_NONE;
}

static PyObject*
map_prev (PyObject* self, PyObject* args) {
	PyObject* key;
	map_class::maybe_E entry;
	uint64_t map_addr;
	if (!PyArg_ParseTuple(args, "Ol", &key, &map_addr))
		return NULL;
	m1 = (map_class*)map_addr;
	entry = m1->previous(key);
	if (entry) {
		PyObject* prev_key = (*entry).first;
		PyObject* prev_value = (*entry).second;
		return Py_BuildValue("(OO)", prev_key, prev_value);
	}
	Py_RETURN_NONE;
}

static PyObject*
map_select (PyObject* self, PyObject* args) {
	uint64_t rank;
	uint64_t map_addr;
	map_class::maybe_E entry;
	if (!PyArg_ParseTuple(args, "ll", &rank, &map_addr))
		return NULL;
	m1 = (map_class*)map_addr;
	entry = m1->select(rank);
	if (entry) {
		PyObject* prev_key = (*entry).first;
		PyObject* prev_value = (*entry).second;
		return Py_BuildValue("(OO)", prev_key, prev_value);
	}
	Py_RETURN_NONE;
}

/**
 * Check if the union and intersection functions are returning the values correctly.
 * Check if the life time of object returned by union and intersect is 
 * not ending. Check how the object equality works for PyObject* in PAM. This is 
 * how we find the duplicate keys and update the value with a function of the values
 * of duplicate keys.
 */
static PyObject*
map_union (PyObject* self, PyObject* args) {
	uint64_t map1_addr;
	uint64_t map2_addr;
	map_class* m2;
	if (!PyArg_ParseTuple(args, "ll", &map1_addr, &map2_addr))
		return NULL;
	m1 = (map_class*)map1_addr;
	m2 = (map_class*)map2_addr;
	try {
		std::cout << "m1 size :: " << m1->size() << endl;
		std::cout << "m2 size: " << m2->size() << endl;
		map_class m3 = m1->map_union(*m1,*m2);
		map_class* m4 = new map_class(m3);
		return Py_BuildValue("l", m4);
	} catch (std::exception& e) {
		std::cout << e.what() << std::endl;	
	}
	Py_RETURN_NONE;
}

static PyObject*
map_intersect (PyObject* self, PyObject* args) {
	uint64_t map1_addr;
	uint64_t map2_addr;
	map_class* m2;
	if (!PyArg_ParseTuple(args, "ll", &map1_addr, &map2_addr))
		return NULL;
	m1 = (map_class*)map1_addr;
	m2 = (map_class*) map2_addr;
	try {
		std::cout << "m1 size :: " << m1->size() << endl;
		std::cout << "m2 size: " << m2->size() << endl;
		map_class m3 = m1->map_intersect(*m1,*m2);
		map_class* m4 = new map_class(m3);
		return Py_BuildValue("l", m4);
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;	
	}
	Py_RETURN_NONE;
}


static PyMethodDef MapMethods[] = {
	{"insert", map_insert, METH_VARARGS, "Insert key into map"},
	{"clear", map_clear, METH_VARARGS, "Clear the contents of the map"},
	{"create", map_create, METH_VARARGS, "create a new pam_map"},
	{"find", map_find, METH_VARARGS, "find value for a given key"},
	{"rank", map_rank, METH_VARARGS, "find the rank of a given key"},
	{"size", map_size, METH_VARARGS, "find size of a given map"},
	{"is_empty", map_is_empty, METH_VARARGS, "find if the map is empty"},
	{"contains", map_contains, METH_VARARGS, "returns true if the map contains the given key"},
	{"next", map_next, METH_VARARGS, "returns a tuple with next key and value pair. If no key is present after the given key then None is returned"},
	{"prev", map_prev, METH_VARARGS, "returns a typle with prev key and value pair. If no key is available that is less than given then None is returned"},
	{"select", map_select, METH_VARARGS, "finds the entry at given rank. Returns None if the given rank is not in the bounds"},
	{"union", map_union, METH_VARARGS, "unions two maps and returns a new map"},
	{"intersect", map_intersect, METH_VARARGS, "finds the intersection of two maps and returns a new map"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef mapmodule = {
	PyModuleDef_HEAD_INIT,
	"map",
	NULL,
	-1,
	MapMethods
};

PyMODINIT_FUNC
PyInit_map (void) {
	return PyModule_Create(&mapmodule);
}

int
main(int argc, char *argv[])
{
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if (program == NULL) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }

    /* Add a built-in module, before Py_Initialize */
    PyImport_AppendInittab("map", PyInit_map);

    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(program);

    /* Initialize the Python interpreter.  Required. */
    Py_Initialize();

    /* Optionally import the module; alternatively,
       import can be deferred until the embedded script
       imports it. */
    PyImport_ImportModule("map");


    PyMem_RawFree(program);
    return 0;
}
