import augmap

class val_class :
    def __init__ (self, i) :
        self.i = i

    def combine (self, b) :
        #print ("combine called")
        return val_class(self.i + b.i)

    def add_aug_val (self, b) :
        return val_class (self.i + b.i)

    def get_empty (self) :
        return val_class(0)

    def __repr__ (self) :
        print (self.i)
        return str (self.i)

class key_class :
    def __init__ (self, i:int) :
        self.i = i

    def __gt__ (self, b) :
        return self.i > b.i

    def __repr__ (self) :
        print (self.i)
        return str(self.i)

    def from_entry (self, val) :
        return val

class restricted_sum :
    def __init__ (self) :
        self.result = 0

    def add_aug_val (self, a) :
        self.result += a.i

    def add_entry (self, tup) :
        b = tup[0].from_entry (tup[1])
        self.result = b.i

#importing the dummy obj class that is necessary for combine function
#to distinguish the null values
augmap.import_def()

mapobj = augmap.augmap_create ()
for i in range (0, 10) :
    augmap.insert (key_class(i), val_class(i), mapobj)

print (augmap.aug_left (key_class(7), mapobj))
print (augmap.aug_right(key_class(2), mapobj))

print (augmap.aug_range (key_class (5), key_class(1), mapobj))
