#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include "pam.h"

static PyObject* pam_dummy = NULL;
static PyObject* dummy_obj = NULL;

/**
* This structure contains two mandatory functions, add_entry and add_aug_val.
* It contains an attribute
*/
struct restricted_sum {
	PyObject* obj;
	restricted_sum (PyObject* obj): obj(obj) {}

	/**
	* When passed to the function range_sum called by an object of typle
	* aug_map_class, the argument to this function is of type pair<key_t, val_t>
	*/
	void add_entry (pair<PyObject*, PyObject*> e) {
		PyObject_CallMethod(obj, "add_entry", "(OO)", e.first, e.second);
	}

	/**
	* When passed to the funciton range_sum called by an object of type
	* aug_map_class, the argument to this function is of type aug_map_class::aug_left
	*/
	void add_aug_val (PyObject* a) {
		PyObject_CallMethod(obj, "add_aug_val", "O", a);
	}
};

struct pymap {
	using key_t = PyObject*;
	using val_t = PyObject*;
	using aug_t = PyObject*;

	static inline bool comp (const key_t& a, const key_t& b) {
		//cout << "compare : " << endl;
		if (a == dummy_obj || b == dummy_obj)
			Py_RETURN_TRUE;
//		PyObject_CallMethod(a,"__repr__", NULL, NULL);
//		PyObject_CallMethod(b,"__repr__", NULL, NULL);
		PyObject* result = PyObject_CallMethod(a,"__gt__", "O", b);
		return PyObject_IsTrue(result);
	}

	/**
	* need to check the string corresponding to the dummy PyObject
	*/
	static inline aug_t combine (const aug_t& a, const aug_t& b) {
		aug_t result = NULL;
//		PyObject_CallMethod(a,"__repr__", NULL, NULL);
//		PyObject_CallMethod(b,"__repr__", NULL, NULL);
		try {
			if (a == dummy_obj) {
				if (b == dummy_obj) {
//					cout << "returning dummy obj" << endl;
					return dummy_obj;
				} else {
//					cout << "returning b obj" << endl;
					PyObject* iden = PyObject_CallMethod(b, "get_empty", NULL, NULL);
					return PyObject_CallMethod(b, "combine", "O", iden);
				}
			} else if (b == dummy_obj) {
//				cout << "returning a obj" << endl;
				PyObject* iden = PyObject_CallMethod(a, "get_empty", NULL, NULL);
				return PyObject_CallMethod(a, "combine", "O", iden);
			} else {
//				cout << "returning a sum b obj" << endl;
				result = PyObject_CallMethod(a, "combine", "O", b);
				Py_INCREF(result);
				return result;
			}
		} catch (exception& e) {
			std::cerr << e.what() << std::endl;
		}
		return result;
	}

	static inline aug_t get_empty () {
		if (dummy_obj == NULL) {
			dummy_obj = PyObject_CallMethod(pam_dummy, "get_dummy", NULL, NULL);
			//PyObject_CallMethod(dummy_obj, "name", NULL, NULL);
		}
		return dummy_obj;
	}

	static inline aug_t from_entry (const key_t& k, const val_t& v) {
		aug_t result = PyObject_CallMethod(k, "from_entry", "O", v);
		Py_INCREF(result);
		return result;
	}
};

using aug_map_class = aug_map<pymap>;

static PyObject*
import_def (PyObject* self, PyObject* args) {
	if (pam_dummy == NULL) {
		pam_dummy = PyImport_ImportModule("pam_dummy");
	}
	if (dummy_obj == NULL) {
		dummy_obj = PyObject_CallMethod(pam_dummy, "get_dummy", NULL, NULL);
		//PyObject_CallMethod(dummy_obj, "name", NULL, NULL);
	}
	Py_RETURN_NONE;
}

static PyObject*
restricted_sum_create (PyObject* self, PyObject* args) {
	PyObject* py_rs_obj;
	if (!PyArg_ParseTuple(args, "O", &py_rs_obj))
		return NULL;
	restricted_sum* rs = new restricted_sum(py_rs_obj);
	return Py_BuildValue("l", rs);
}

static PyObject*
augmap_create (PyObject* self, PyObject* args) {
	aug_map_class* m = new aug_map_class();
	return Py_BuildValue("l",m);
}

static PyObject*
aug_left (PyObject* self, PyObject* args) {
	//cout << "aug_left called " << endl;
	uint64_t addr;
	PyObject* key;
	if (!PyArg_ParseTuple(args, "Ol", &key, &addr))
		return NULL;
	aug_map_class* m = (aug_map_class*)addr;
	PyObject* result = m->aug_left(key);
	Py_INCREF(result);
	return result;
}

static PyObject*
aug_right (PyObject* self, PyObject* args) {
	uint64_t addr;
	PyObject* key;
	if (!PyArg_ParseTuple(args, "Ol", &key, &addr))
		return NULL;
	aug_map_class* m = (aug_map_class*)addr;
	PyObject* result = m->aug_right(key);
	Py_INCREF(result);
	return result;
}

static PyObject*
aug_range (PyObject* self, PyObject* args) {
	uint64_t addr;
	PyObject* l;
	PyObject* r;
	if (!PyArg_ParseTuple(args, "OOl", &l, &r, &addr))
		return NULL;
	aug_map_class* m = (aug_map_class*)addr;
	PyObject* result = m->aug_range(l,r);
	Py_INCREF(result);
	return result;
}

/**
* The arguments for this function are
* l : left end of the range
* r : right end of the range
* rs : an instance of class restricted sum
* addr : address of the aug_map_class object
* returns PyObject of type aug_val
*/
static PyObject*
aug_range_sum (PyObject* self, PyObject* args) {
	uint64_t* addr;
	uint64_t* rs_addr;
	PyObject *l, *r;
	if (!PyArg_ParseTuple(args, "OOll", &l, &r, &rs_addr, &addr))
		return NULL;
	aug_map_class* m = (aug_map_class*)addr;
	restricted_sum* rs = (restricted_sum*)rs_addr;
	m->range_sum (l, r, *rs);
	Py_RETURN_NONE;
}

static PyObject*
aug_select (PyObject* self, PyObject* args) {
	PyObject* func;
	uint64_t addr;
	aug_map_class::maybe_E entry;
	if (!PyArg_ParseTuple(args, "O", &func, &addr))
		return NULL;
	auto f = [func] (PyObject* v) {
		return PyObject_IsTrue( PyObject_CallMethod(func, "__call__", "O", v));
	};
	aug_map_class* m = (aug_map_class*)addr;

	entry = m -> aug_select(f);
	if (!!entry) {
		PyObject* prev_key = (*entry).first;
		PyObject* prev_value = (*entry).second;
		Py_INCREF(prev_key);
		Py_INCREF(prev_value);
		return Py_BuildValue("(OO)", prev_key, prev_value);
	}
	Py_RETURN_NONE;
}

static PyObject*
insert(PyObject* self, PyObject* args) {
	PyObject* key;
	PyObject* value;
	uint64_t pam_map_addr;

	if (!PyArg_ParseTuple(args, "OOl", &key, &value, &pam_map_addr))
		return NULL;
	aug_map_class* m1 = (aug_map_class*)(pam_map_addr);
	try {
		m1->insert(std::make_pair(key, value));
		Py_INCREF(key);
		Py_INCREF(value);
	} catch (exception& e) {
		std::cout << e.what() << std::endl;
	}
	return Py_BuildValue("O", key);
}

static PyMethodDef MapMethods[] = {
	{"import_def", import_def, METH_VARARGS, NULL},
	{"restricted_sum_create", restricted_sum_create, METH_VARARGS, NULL},
	{"augmap_create", augmap_create, METH_VARARGS, NULL},
	{"aug_left", aug_left, METH_VARARGS, NULL},
	{"aug_right", aug_right, METH_VARARGS, NULL},
	{"aug_range", aug_range, METH_VARARGS, NULL},
	{"aug_range_sum", aug_range_sum, METH_VARARGS, NULL},
	{"aug_select", aug_select, METH_VARARGS, NULL},
	{"insert", insert, METH_VARARGS, NULL},
	//{"is_empty", is_empty, METH_VARARGS, NULL},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef augmapmodule = {
	PyModuleDef_HEAD_INIT,
	"augmap",
	NULL,
	-1,
	MapMethods
};

PyMODINIT_FUNC
PyInit_augmap (void) {
	return PyModule_Create(&augmapmodule);
}

int
main(int argc, char *argv[])
{
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if (program == NULL) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }

    /* Add a built-in module, before Py_Initialize */
    PyImport_AppendInittab("augmap", PyInit_augmap);

    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(program);

    /* Initialize the Python interpreter.  Required. */
    Py_Initialize();

    /* Optionally import the module; alternatively,
       import can be deferred until the embedded script
       imports it. */
    PyImport_ImportModule("augmap");


    PyMem_RawFree(program);
    return 0;
}
